# cTAKES Friendly Web UI

This is a web viewer to be used with https://github.com/GoTeamEpsilon/ctakes-rest-service.

## Install

The code can be served up by any modern web server and viewed in any modern web browser.

## Dependencies

- jQuery
- jQueryUI

## Usage

_this project isn't done, please don't use it quite yet :)_

See `index.html` for an example.

## Demonstration

![demo](demo.png)

## TODOs

- Clean up css names
- Testing on other browsers
- Write unit tests

## LICENSE

MIT
