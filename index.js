 /**
  * Creates an instance of cTAKESFriendlyWebUi for use in any web-based EMR that is using the
  * greater https://github.com/GoTeamEpsilon/cTAKES-Intelligent-Chart-Summarization-Solution
  * solution.
  *
  * @constructor
  * @param {Object} jQueryInstance - A reference to jQuery with jQueryUI to make unit testing easier.
  * @param {string} rawText - The original corpus sent to cTAKES.
  * @param {Array<Object>} rawData - The raw cTAKES output.
  */
function cTAKESFriendlyWebUi(jQueryInstance, rawText, rawData) {
  var self = this;

  self._text = rawText;
  self._rawData = rawData;

  /**
   * This structure represents a "working" around for this code to sanitize and classify
   * the concept mentions.
   *
   * @TODO: This structure starts out with a string field called "code" and is later converted
   *        to an Array<String> field called "codesWithType". Should be consistent in terms of
   *        typing and consistency here.
   */
  self._unprocessedConcepts = [];

  /**
   * Final structure used by jQuery to "paint" the view.
   */
  self._processedConcepts = [];

  /**
   * Map of known cTAKES concept mention types. These colors were hand picked to be light
   * so that black font could easily read overtop them.
   *
   * @TODO: Add an `options` parameter to the constructor to configure this, if desired.
   */
  self._mentionMap = {
    MedicationMention: {
      color: '#f3ffc8',
      name: 'Medication'
    },
    SignSymptomMention: {
      color: '#b2c4f4',
      name: 'Sign/Symptom'
    },
    DiseaseDisorderMention: {
      color: '#a4e0f4',
      name: 'Disease/Disorder'
    },
    AnatomicalSiteMention: {
      color: '#ffeccc',
      name: 'Anatomical Site'
    },
    ProcedureMention: {
      color: '#ffcaca',
      name: 'Procedure'
    }
  };

  /**
   * These constants are used to notate noteworthy concept mentions and metadata at
   * specific positions in the output HTML. Note that if the original corpus contains
   * STRING_SPLIT_MARKER, CONCEPT_MARKER and NEW_LINE_MARKER, it will be replaced
   * with a single space.
   */
  var STRING_SPLIT_MARKER = '~';
  var NEW_LINE_MARKER = '}'
  var CONCEPT_MARKER = '{';
  var MARGIN_OF_ERROR_INDICATOR = '*';
  var CODES_DATA_SEPARATOR = ':';

  /**
   * This method does the following:
   *    1. Replaces STRING_SPLIT_MARKER, CONCEPT_MARKER, and NEW_LINE_MARKER with a single
   *       space to avoid issues with the algorithm. While this is not the most elegant
   *       approach because the original text will be modified, it is a minor compromise as
   *       the code is cleaner and faster as a result.
   *
   *    2. Replaces \n with a NEW_LINE_MARKER that will be used for line breaks after
   *       processing is complete. Note that a newLineEscapeCharacterReplacementCharacter
   *       with the value of ' ' will be added to replaced newlines not mess up the offset
   *       math. The output of this will be as followed:
   *
   *           - BEFORE: 'the patient \n is feeling better'
   *           - AFTER:  'the patient  } is feeling better'
   *
   *         ...because '}', the NEW_LINE_MARKER, has only a length of 1 and '\n' has a length
   *            of 2, the added newLineEscapeCharacterReplacementCharacter ' ' evens out the
   *            total size of the text and does not change the viewing experience. Note that
   *            this could be solved with double character markers, but that is not consistents
   *            with the other markers and is less elegant.
   *
   *    3. Cleans up unicode entities left over from the raw cTAKES data.
   */
  self._sanitizeRawText = function() {
    var replacementCharacter = ' ';
    var newLineEscapeCharacterReplacementCharacter = replacementCharacter;

    // TODO: Not sure this is needed?
    self._text = decodeURIComponent(self._text);

    // Clean up...
    self._text = self._text.replace(new RegExp(STRING_SPLIT_MARKER, 'g'), replacementCharacter);
    self._text = self._text.replace(new RegExp(CONCEPT_MARKER, 'g'), replacementCharacter);
    self._text = self._text.replace(new RegExp(NEW_LINE_MARKER, 'g'), replacementCharacter);
    self._text = self._text.replace(
      new RegExp(/[\r\n]+/, 'g'), newLineEscapeCharacterReplacementCharacter + NEW_LINE_MARKER);
    self._text = self._text.replace(
      /&(?:#x[a-f0-9]+|#[0-9]+|[a-z0-9]+);?/ig, replacementCharacter);
  };

  /**
   * This method removes duplicate concept mentions as well as handles the case where
   * a concept mention shares the exact begin and end (results in a merge).
   */
  self._mergeIdenticallyPositionedConcepts = function() {
    self._unprocessedConcepts = self._rawData.reduce(function(memo, concept) {
      var isDuplicate = false;

      memo.forEach(function(v) {
        if (v.begin === concept.begin && v.end === concept.end && v.code === concept.code) {
          isDuplicate = true;
        }
      });

      if (!isDuplicate) {
        memo.push(concept);
      }

      return memo;
    }, []);

    // This code block indexes the concept mentions by sum(begin, end). This is a clever means of
    // combining codes and types and is only temporary.
    var indexedConcepts = {}
    for (var conceptIter = 0; conceptIter < self._unprocessedConcepts.length; conceptIter++) {
      var formattedDataCode = self._unprocessedConcepts[conceptIter].type + CODES_DATA_SEPARATOR +
                              self._unprocessedConcepts[conceptIter].scheme + CODES_DATA_SEPARATOR +
                              self._unprocessedConcepts[conceptIter].code;
      if (!indexedConcepts[self._unprocessedConcepts[conceptIter].begin + self._unprocessedConcepts[conceptIter].end]) {
        indexedConcepts[self._unprocessedConcepts[conceptIter].begin + self._unprocessedConcepts[conceptIter].end] = {
          'codesWithType': [formattedDataCode],
          'begin': self._unprocessedConcepts[conceptIter].begin,
          'end': self._unprocessedConcepts[conceptIter].end
        };
      } else {
        indexedConcepts[self._unprocessedConcepts[conceptIter].begin +
            self._unprocessedConcepts[conceptIter].end].codesWithType.push(formattedDataCode);
      }
    }

    // As noted above, the indexed structure was just used for merging purposes and now that it's
    // done it's job, we can flatten the structure back into the Array<Object>s for the class member
    // "_unprocessedConcepts".
    var deIndexedConcepts = []
    Object.keys(indexedConcepts).forEach(function(k) {
      deIndexedConcepts.push(indexedConcepts[k]);
    });

    // This is how you delete an array and copy new values in JS.
    self._unprocessedConcepts.splice(0);
    self._unprocessedConcepts = deIndexedConcepts;
  };

  /**
   * This method does the heavy lifting to get member _processedConcepts established. Any
   * immediate colocated concept mentions will be merged. A special field 'mergeWithParent'
   * is set to true for parents to:
   *
   *   1) Use all codes from children, marked with MARGIN_OF_ERROR_INDICATOR
   *   2) Expand their width (via begin and end of children, in case there are multiple related terms involved)
   */
  self._mergeRelatedConcepts = function() {
    for (var conceptIter = 0; conceptIter < self._unprocessedConcepts.length; conceptIter++) {
      if (self._unprocessedConcepts[conceptIter-1] && self._unprocessedConcepts[conceptIter-1].mergeWithParent) {

        // This loop takes care of marking child codes with a margin of error character.
        for(var codesIter = 0; codesIter < self._unprocessedConcepts[conceptIter-1].codesWithType.length; codesIter++) {
          if (self._unprocessedConcepts[conceptIter-1].codesWithType[codesIter].indexOf(MARGIN_OF_ERROR_INDICATOR) === -1) {
            self._unprocessedConcepts[conceptIter-1].codesWithType[codesIter] = MARGIN_OF_ERROR_INDICATOR +
                                                     self._unprocessedConcepts[conceptIter-1].codesWithType[codesIter];
          }
        }

        // Caches all related codes up until this point in case there are many children.
        self._unprocessedConcepts[conceptIter].codesWithType = self._unprocessedConcepts[conceptIter].codesWithType
          .concat(self._unprocessedConcepts[conceptIter-1].codesWithType)

        // Determine if current concept mention should be included in a subsequent concept mention (parent).
        if ((self._unprocessedConcepts[conceptIter+1] &&
            self._unprocessedConcepts[conceptIter+1].end === self._unprocessedConcepts[conceptIter].end) ||
            (self._unprocessedConcepts[conceptIter+1] &&
             self._unprocessedConcepts[conceptIter+1].begin === self._unprocessedConcepts[conceptIter].begin)) {
          self._unprocessedConcepts[conceptIter]['mergeWithParent'] = true
        } else {
          // Applies parent codes and all cached codes (children).
          var concatCodes = self._unprocessedConcepts[conceptIter].codesWithType
            .concat(self._unprocessedConcepts[conceptIter-1].codesWithType)

          // Removes any duplicate codes.
          var uniqueCodes = [];
          jQueryInstance.each(concatCodes, function(codeIter, code) {
            if (jQueryInstance.inArray(code, uniqueCodes) === -1) {
              uniqueCodes.push(code);
            }
          });

          // Because child concepts can extend out pass the end of the parent, favor their end position if justified.
          var finalEndPosition = (self._unprocessedConcepts[conceptIter].end > self._unprocessedConcepts[conceptIter-1].end)
           ? self._unprocessedConcepts[conceptIter].end
           : self._unprocessedConcepts[conceptIter-1].end

          // Done processing the concept mention, add it to the final structure.
          self._processedConcepts.push({
            'codesWithType': uniqueCodes,
            'begin': self._unprocessedConcepts[conceptIter-1].begin,
            'end': finalEndPosition
          });
        }
      } else {
        // Determine if current concept mention should be included in a subsequent concept mention (parent).
        if ((self._unprocessedConcepts[conceptIter+1] &&
            self._unprocessedConcepts[conceptIter+1].end === self._unprocessedConcepts[conceptIter].end) ||
            (self._unprocessedConcepts[conceptIter+1] &&
             self._unprocessedConcepts[conceptIter+1].begin === self._unprocessedConcepts[conceptIter].begin)) {
          self._unprocessedConcepts[conceptIter]['mergeWithParent'] = true
        } else {
          // Concept does not related to anything else, simply add it to the final structure as is.
          self._processedConcepts.push({
            'codesWithType': self._unprocessedConcepts[conceptIter].codesWithType,
            'begin': self._unprocessedConcepts[conceptIter].begin,
            'end': self._unprocessedConcepts[conceptIter].end
          });
        }
      }
    }
  };

  /**
   * This sort function mutates our member _unprocessedConcepts so that it is perfectly
   * ordered from left to right (in terms of the corpus) because our algorithm expects
   * the data to be ordered in this way for it to work.
   */
  self._sortConcepts = function() {
    self._unprocessedConcepts = self._unprocessedConcepts.sort(function(a, b) {
      if (a.begin < b.begin)
        return -1;
      if (a.begin > b.begin)
        return 1;

      return 0;
    });
  };

  /**
   * Places special markers around raw text to indicate that it is an (ordered) concept mention
   * and will soon have data. Note that a textOffsetCount is needed to been the running count
   * of how much space has been added to the left because the markers change the size of the
   * _text member string.
   *
   * @example: If he changes positions frequently or ~|stretches~ his ~|legs~, this seems to help.
   */
  self._insertConceptMarkers = function() {
    var textOffsetCount = 0;
    self._processedConcepts.forEach(function(v) {
      var leftChunk = self._text.slice(0, v.begin + textOffsetCount);
      var middleChunk = STRING_SPLIT_MARKER + CONCEPT_MARKER + self._text.slice(
        v.begin + textOffsetCount, (v.end + textOffsetCount)) + STRING_SPLIT_MARKER;
      var rightChunk = self._text.slice(v.end + textOffsetCount);
      textOffsetCount += 3; // there are 3 characters added (~||)
      self._text = leftChunk.concat(middleChunk).concat(rightChunk);
    });
  };

  /**
   * Fills in marked areas from the _insertConceptMarkerIntoText method with actual HTML.
   *
   * @example: (...note that this example is formatted for space, there are no line breaks in reality...)
   *           <span data-codes="MedicationMention:SNOMEDCT_US:5540006,
   *                             MedicationMention:RXNORM:1895,
   *                             *MedicationMention:SNOMEDCT_US:48698004,
   *                             *MedicationMention:SNOMEDCT_US:373304005,
   *                             *SignSymptomMention:SNOMEDCT_US:115568009">calcium channel blocker</span>
   */
  self._replaceConceptMarkersWithData = function() {
    var textSplitByMarkers = self._text.split(STRING_SPLIT_MARKER);

    var conceptIter = 0;
    textSplitByMarkers.forEach(function(chunk, index) {
      if (chunk.indexOf(CONCEPT_MARKER) !== -1) {
        chunk = chunk.replace(
          CONCEPT_MARKER, '<span data-codes="' + self._processedConcepts[conceptIter].codesWithType.join(',') + '">');
        chunk = chunk.concat('</span>');
        textSplitByMarkers[index] = chunk;
        conceptIter++;
      }
    });

    self._text = textSplitByMarkers.join('');
  };

  /**
   *
   */
  self._replaceNewLineMarkersWithHtmlBreaks = function() {
    self._text = self._text.replace(new RegExp(NEW_LINE_MARKER, 'g'), '<br/>');
  };

  /**
   * jQuery-driven method that wires up on hover handlers for each concept mention so that
   * the parsed out "data-codes" can be listed in the bottom right viewer.
   */
  self._wireUpConceptHandlers = function() {
    jQueryInstance('.ui-container .left .text span').on('mouseover', function() {
      jQueryInstance('.ui-container .right .stats .list').empty();
      var spanSelf = this;
      var codes = jQueryInstance(spanSelf).data('codes').split(',');

      codes.forEach(function(v) {
        var hasMarginOfError = v.indexOf(MARGIN_OF_ERROR_INDICATOR) > -1;
        var codesSplit = v.split(CODES_DATA_SEPARATOR);
        var codeString = (hasMarginOfError ? MARGIN_OF_ERROR_INDICATOR : '') + codesSplit[1] + ' ' + codesSplit[2];
        jQueryInstance('.ui-container .right .stats .list').append('<li>' + codeString  + '</li>');
      });
    });
  };

  /**
   * jQuery-driven method that colors in concept mentions with a background color
   * corresponding with the type that is specified in the "data-codes" attribute or
   * two colors if there are two types.
   *
   * @TODO: Is it possible for there to be 3 types in one mention?
   * @TODO: Does linear-gradient need to have a vendor prefix?
   */
  self._colorInConceptSpans = function() {
    jQueryInstance('.ui-container .left .text span').each(function() {
      var spanSelf = this;
      var uniqueMentions = {};
      jQueryInstance(spanSelf).data('codes').split(',').forEach(function(v) {
        var codesSplit = v.split(CODES_DATA_SEPARATOR);
        uniqueMentions[codesSplit[0].replace(MARGIN_OF_ERROR_INDICATOR, '')] = null;
      });

      switch (Object.keys(uniqueMentions).length) {
        case 1:
          var colorKey = Object.keys(uniqueMentions)[0]
          jQueryInstance(spanSelf).css('background', self._mentionMap[colorKey].color);
          break;
        case 2:
          var colorKeyOne = Object.keys(uniqueMentions)[0]
          var colorKeyTwo = Object.keys(uniqueMentions)[1]
          jQueryInstance(spanSelf).css(
            'background-image',
            'linear-gradient(0deg, ' + self._mentionMap[colorKeyOne].color + ' 50%, ' +
            self._mentionMap[colorKeyTwo].color + ' 50%)')
          break;
      }
    });
  };

  /**
   * The color key to the top right of the screen.
   */
  self._displayColorKey = function() {
    var keyList = jQueryInstance('.ui-container .right .key .list');
    Object.keys(self._mentionMap).forEach(function(v) {
      keyList.append('<li style="background:' + self._mentionMap[v].color + '">' + self._mentionMap[v].name  + '</li>');
    });

    keyList.append('<li>* = slightly higher margin of error</li>');
  };

  /**
   * A nice table that shows the codes as they came in, partition by "type" tabs.
   */
  self._displayRawCodes = function() {
    var conceptsPartitionedByType = {}
    rawData.forEach(function(v) {
      if (!conceptsPartitionedByType.hasOwnProperty(v.type)) {
        conceptsPartitionedByType[v.type] = [v]
      } else {
        conceptsPartitionedByType[v.type].push(v)
      }
    });

    Object.keys(self._mentionMap).forEach(function(v) {
      var codeTabList = jQueryInstance('.ui-container .all-codes .tabs .list')
      codeTabList.append('<li style="background:' + self._mentionMap[v].color + ';"><a href="#' + v + '">' + self._mentionMap[v].name + '</a></li>');
      codeTabList.after('<div id="' + v + '"><table class="codes"></table></div>');

      var codeBodyTable = jQueryInstance('.ui-container .all-codes .tabs #' + v + ' .codes');
      codeBodyTable.append('<tr><th>Code</th><th>Text</th></tr>');
      jQueryInstance.each(conceptsPartitionedByType[v], function(iterator, concept) {
        codeBodyTable.append('<tr><td>'+ concept.scheme + ' ' + concept.code +'</td><td>' + concept.text + '</td></tr>');
      });
    });

    // Dependency on jQuery UI here.
    jQueryInstance(".ui-container .all-codes .tabs").tabs();
  };

  /**
   * Shows the processed HTML on the screen.
   */
  self._displayProcessedText = function() {
    jQueryInstance('.ui-container .left .text').html('<p>' + self._text + '</p>');
  };

  /**
   * Public method that calls, in order, the methods needed to run the solution.
   */
  self.processAndDisplayConcepts = function() {
    self._sanitizeRawText();
    self._mergeIdenticallyPositionedConcepts();
    self._sortConcepts();
    self._mergeRelatedConcepts();
    self._insertConceptMarkers();
    self._replaceConceptMarkersWithData();
    self._replaceNewLineMarkersWithHtmlBreaks();

    jQueryInstance(document).ready(function() {
      self._displayProcessedText();
      self._displayColorKey();
      self._displayRawCodes();
      self._colorInConceptSpans();
      self._wireUpConceptHandlers();
    });
  };
};
